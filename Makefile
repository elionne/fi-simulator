
ploters = $(wildcard images/*.plot.rb)
graphs = $(patsubst %.plot.rb, %.svg, $(ploters))

images_tikz = $(wildcard images/*.tex)
images_pdf  = $(patsubst %.tex, %.pdf, $(images_tikz))
images_svg  = $(patsubst %.tex, %.svg, $(images_tikz))

%.svg: %.pdf
	pdf2svg $< $@

%.pdf: %.tex
	pdflatex -interaction batchmode  -output-directory $(dir $@) $<


%.svg: %.plot.rb *.rb
	ruby -I. $<

README.html: README.rst $(graphs) $(images_svg)
	rst2html5 $< $@

clean:
	rm $(graphs) $(images_svg) $(images_pdf) README.html

default: README.html
