require 'oscillator'
require 'phase_frequency_detector'
require 'low_filter'
require 'signal'

require 'gnuplot'

osc1 = Fi::Oscillator.new
osc2 = Fi::Oscillator.new(-Math::PI/2)

pfd = Fi::PhaseFrequencyDetector.new

i1 = Fi::Signal.new
i2 = Fi::Signal.new

data = []

f1 = 0.07
f2 = 0.065

200.times do

  i1.signal = osc1.run(f1).first
  i2.signal = osc2.run(f2).first

  phase = case pfd.run(i1, i2)
  when :in_phase then 0
  when :pos_phase then +1
  when :neg_phase then -1
  else 0
  end

  data << [i1.signal, i2.signal, phase]
end

def output_filename(out_ext)
  ext = '.plot.rb'
  dir = File.dirname(__FILE__)
  base = File.basename(__FILE__, ext)

  File.join(dir, base + out_ext)
end


Gnuplot.open do |gp|
  Gnuplot::Plot.new(gp) do |plot|

    plot.yrange  '[-5:3]'
    plot.y2range '[-1:4]'
    plot.xrange  '[0:200]'
    plot.xtics   'nomirror'

    plot.unset 'y2tics'
    plot.unset 'ytics'

    plot.output output_filename('.svg')
    plot.terminal 'svg'

    i1, i2, phase = data.transpose

    base_signal = Gnuplot::DataSet.new( i1 ) do |ds|
      ds.title = "signal #{f1}"
      ds.with  = 'lines'
    end

    f_shifted_signal = Gnuplot::DataSet.new( i2 ) do |ds|
      ds.title = "signal #{f2}"
      ds.with  = 'lines'
    end

    bin_phase = Gnuplot::DataSet.new( phase ) do |ds|
      ds.title =  'phase binaire'
      ds.axes  = 'x1y2'
      ds.with  = 'lines'
    end

    plot.data = [ base_signal, f_shifted_signal, bin_phase ]
  end
end
