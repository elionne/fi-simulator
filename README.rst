Démodulateur de phase du signal d’horloge France Inter
======================================================

Modulation de phase
-------------------

L’idée est de créer un générateur de signal modulé en phase.
La modulation de phase utilisé par France Inter est représenté ci-dessous.
`Plus d’information`_ peuvent être trouvés sur le web.

.. _Plus d’information: http://tvignaud.pagesperso-orange.fr/am/allouis/allouis-heure.htm

::

  ruby phase_modulator.plot.rb

.. image:: images/phase_modulator.svg

Comparateur de phase à trois états
----------------------------------

Le comparateur de phase à trois est facile à implémenter dans un programme et il permet également d’avoir une sortie linéaire de la phase.

La valeur moyenne de la sortie est proportionnelle à la phase sur l’intervale :math:`[-2\cdot\pi:2\cdot\pi]`.

Le principe est décrit par le schémas suivant :

.. image:: images/three_states.svg
    :width: 400px

Voir le document :file:`PLL.pdf`.

::

  ruby phase_detector.plot.rb


.. image:: images/phase_detector.svg


Filtre de Butterworth passe-bas pour la valeur moyenne
------------------------------------------------------

J’utilise un filtre de Butterworth pour récupérer la valeur moyenne.
Dans mes précédant tests, j’avais utilisé un filtre par moyenne mobile, mais le résultat était médiocre.

Le filtre de Butterworth est facilement implémentable.
C’est un filtre RII [1]_ dans les paramètres sont déterminés par la fonction ``butter``.
Voir `Installer Octave et ses dépendances`_.

::

  ruby phase_detector_filtered.plot.rb

.. image:: images/phase_detector_filtered.svg

Installer Octave et ses dépendances
-----------------------------------

Pour trouver les coefficients du filtre passe-bas premier ordre j’ai besoin d’avoir les coéfficients du polynome issue de la transformée en Z [1]_.

La fonction ``butter`` de Octave permet de donner le coefficients de l’équation de récurence pour un filtre Butterworth.

La documentation des fonctions filter_, bilinear_ et butter_ de Matlab sont utiles.

.. _filter: https://fr.mathworks.com/help/matlab/ref/filter.html#buagwwg-2
.. _bilinear: https://fr.mathworks.com/help/signal/ref/bilinear.html
.. _butter: https://fr.mathworks.com/help/signal/ref/butter.html

J’ai utilisé la paquet déjà prêt de aur octave-signal.

::

  yaourt -S octave-signal

Puis dans octave faire ::

  pkg load signal

Dans mes tests j’utilise :math:`f_n` qui représente la fréquence normalisé sans dimension définit par :math:`f_n = \frac{f}{f_e}` ou :math:`f` est la fréquence du signal et :math:`f_e` la fréquence d’échantiollange.

J’ai choisi arbitrairement :math:`f_n = 0.07` pour mes tests.
Une autre fréquence légèrement différente pour observer un léger déphase continue et de :math:`0.065`.

Commandes ``gnuplot`` à la traces
---------------------------------

.. [1] http://www.f-legrand.fr/scidoc/docimg/numerique/filtre/rii/rii.html

