require 'clock_data'
require 'phase_signal'
require 'phase_modulator'
require 'signal'
require 'phase_frequency_detector'
require 'low_filter'
require 'oscillator'


require 'gnuplot'

clock_data = Fi::ClockData.new.data
phase = Fi::PhaseSignal.new(clock_data)

modulator = Fi::PhaseModulator.new 1/162.0e3

modulated = Fi::Signal.new
demodulated = Fi::Signal.new
main = Fi::Signal.new

pfd = Fi::PhaseFrequencyDetector.new
butterworth_lp = Fi::LowFilter.new

osc1 = Fi::Oscillator.new
f1 = 0.05

data = []
phi = 0

(0..1000).each do |i|

  # clock_signal =  clock_data.signal
  # phase_mod = modulator.signal_sample(i, clock_signal)
  # clock_signal = demodulator.signal_sample(i, phase_mod)


  _, modulated.signal, phi = modulator.signal_sample(i*301, phase)
  _, main.signal = osc1.run(f1)

  phase_error = case pfd.run(modulated, main)
  when :in_phase then 0
  when :pos_phase then +2*Math::PI
  when :neg_phase then -2*Math::PI
  else 0
  end

  demodulated.signal = butterworth_lp.filter(phase_error)

  puts "% 0.6f % 0.6f % 0.6f % 0.6f %i %i" % [demodulated.signal, modulated.signal, main.signal, phi, modulated.edge, main.edge].map{ |f| f.round 6 }

end

