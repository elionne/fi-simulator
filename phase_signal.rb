module Fi

  class PhaseSignal

    attr_reader :samples

    ZERO_DURATION = 0.1
    ONE_DURATION = 0.2

    def initialize(data, samples = 4)
      @samples = samples
     # @signal = signal(data)
      @data = data
      @symbol_duration = 1
    end

    def signal(data)
      data.flat_map &proc { |a| schema(a) }
    end

    def at(t)
      signal = @data[t / @symbol_duration]
      case signal
      when 1 then one(t.modulo @symbol_duration)
      when 0 then zero(t.modulo @symbol_duration)
      else silent(t.modulo @symbol_duration)
      end
    end

    def zero(t)
      p = t/ZERO_DURATION
      case t
      when 0..ZERO_DURATION then sub_wave(p)
      else silent(t)
      end
    end

    def one(t)
      p = t.modulo(ZERO_DURATION)/ZERO_DURATION
      case t
      when 0..ONE_DURATION then sub_wave(p)
      else silent(t)
      end
    end

    def silent(t)
      0
    end

    def wave
      samples.times.map { |p| sub_wave(p/samples.to_f) }
    end

    def sub_wave(p)
      duty_cycle = 0.5
      max = 1
      min = -1

      # Génère une signal triangle
      #     max
      #    /\
      #   /  \
      # _/    \    p=1
      #p=0     \    /
      #         \  /
      #          \/
      #           min
      #
      #  |c1| c2 |c3|
      #
      # 0 < p ≤ 1
      # 0 ≤ duty_cycle < 1
      # 0 < max < n
      # -n < min < 0
      #
      # max_x = duty_cycle / 2
      # min_x = 1 - duty_cycle / 2
      #
      # y = ax + b
      case
      when p <= duty_cycle / 2
        # c1 :
        #
        # y = max / duty_cycle / 2 * x => 2·x·max/duty_cycle

        2 * max * p / duty_cycle
      when p <= 1 - duty_cycle / 2
        # c2 :
        #
        # y1(max_x) = max
        # y2(min_x) = min
        #
        # y2 - y1 = min - max
        # a·(1-d/2) + b - a·d/2 - b = min - max
        # a = (min - max)/(1 - d)
        #
        # b = y1 - a·max_x

        # y = a·x + y1 - a·max_x
        # y = a·(x - max_x) + y1
        #
        # y = (min - max)/(1 - d)(x - d/2) + max
        (p - duty_cycle/2) * (min - max) / (1 - duty_cycle) + max
      else
        # c3 :
        #
        # y1(min_x) = min
        # y2(1) = 0
        #
        # a = - min / (1 - 1 + d/2) = -2·min / d
        # b = 0 - a·1
        #
        # y = ax + a
        # y = (1-x)·2·min/d
        2*min/duty_cycle * (1 - p)
      end

    end

  end
end

