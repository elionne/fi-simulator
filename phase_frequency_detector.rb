module Fi
  class PhaseFrequencyDetector
    attr_reader :state
    attr_reader :phase_accumulator

    def initialize
      @state = :in_phase
    end

    def run(a, b)

      case state
      when :in_phase
        @state = :pos_phase if a.edge.positive?
        @state = :neg_phase if b.edge.positive?
        @state = :in_phase  if a.edge.positive? && b.edge.positive?
      when :pos_phase
        @state = :in_phase if b.edge.positive?
      when :neg_phase
        @state = :in_phase if a.edge.positive?
      else
        @state
      end

      @state
    end

  end
end
