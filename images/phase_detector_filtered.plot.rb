require 'oscillator'
require 'phase_frequency_detector'
require 'low_filter'
require 'signal'

require 'gnuplot'

osc1 = Fi::Oscillator.new
osc2 = Fi::Oscillator.new(-Math::PI/2)

pfd = Fi::PhaseFrequencyDetector.new

i1 = Fi::Signal.new
i2 = Fi::Signal.new

phi = 0
butterworth_lp = Fi::LowFilter.new

data = []

f1 = 0.07
f2 = 0.065

50000.times do |i|

  i1.signal = osc1.run(f1).first
  i2.signal = osc2.run(f2 + phi * 0.1).first

  phase = case pfd.run(i1, i2)
  when :in_phase then 0
  when :pos_phase then +1
  when :neg_phase then -1
  else 0
  end

  phi = butterworth_lp.filter phase

  #puts "%f %f %f" % [i1.signal, i2.signal, phase]
  data << [i1.signal, i2.signal, phi] if i > 49000

end

def output_filename(out_ext)
  ext = '.plot.rb'
  dir = File.dirname(__FILE__)
  base = File.basename(__FILE__, ext)

  File.join(dir, base + out_ext)
end


Gnuplot.open do |gp|
  Gnuplot::Plot.new(gp) do |plot|

    plot.yrange  '[-5:3]'
    plot.y2range '[-1:4]'
    plot.xrange  '[800:1000]'
    plot.xtics   'nomirror'

    plot.unset 'y2tics'
    plot.unset 'ytics'

    plot.output output_filename('.svg')
    plot.terminal 'svg'

    i1, i2, phi = data.transpose

    base_signal = Gnuplot::DataSet.new( i1 ) do |ds|
      ds.title = "signal #{f1}"
      ds.with  = 'lines'
    end

    f_shifted_signal = Gnuplot::DataSet.new( i2 ) do |ds|
      ds.title = "signal #{f2}"
      ds.with  = 'lines'
    end

    bin_phase = Gnuplot::DataSet.new( phi ) do |ds|
      ds.title =  'phase binaire'
      ds.axes  = 'x1y2'
      ds.with  = 'lines'
    end

    plot.data = [ base_signal, f_shifted_signal, bin_phase ]
  end
end
