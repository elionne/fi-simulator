module Fi

  class ClockData

    attr_reader :cycle

    def output(cycle)
      @cycle = cycle

      case cycle
      when 0..15  then reserved
      when 16..18 then daylight_saving[cycle-16]
      when 19     then reserved
      when 20     then start
      when 21..27 then minute[cycle-21]
      when 28     then minute_odd
      when 29..34 then hour[cycle-29]
      when 35     then hour_odd
      when 36..41 then day_of_month[cycle-36]
      when 42..44 then day_of_week[cycle-42]
      when 45..49 then month[cycle-45]
      when 50..57 then year[cycle-50]
      when 58     then date_odd
      when 59     then reset
      end
    end

    def data
      signal = []
      60.times { |cycle| signal << output(cycle) }
      signal
    end

    def initialize(time = Time.now)
      @cycle = 0
      @time = time
    end

    private

    def daylight_saving
      dst = @time.dst? ? 1 : 0
      [0, dst, 1-dst]
    end

    def minute
      bcd(@time.min)
    end

    def hour
      bcd(@time.hour)
    end

    def day_of_week
      bcd(@time.hour)
    end

    def day_of_month
      bcd(@time.day)
    end

    def month
      bcd(@time.month)
    end

    def year
      bcd(@time.year)
    end

    def minute_odd
      parity(minute.to_s(2))
    end

    def hour_odd
      parity(hour.to_s(2))
    end

    def date_odd
      parity(year.to_s(2) + month.to_s(2) + day_of_week.to_s(2) + day_of_month.to_s(2))
    end

    def reserved
      rand(0..1)
    end

    def start
      1
    end

    def reset
      0
    end

    def parity(bin_string)
      bin_string.count('1').odd? ? 1 : 0
    end

    def bcd(num)
      dozen, unit = num.divmod 10
      (dozen << 4) + unit
    end

  end
end
