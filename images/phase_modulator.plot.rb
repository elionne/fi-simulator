require 'clock_data'
require 'phase_signal'
require 'phase_modulator'

require 'gnuplot'

clock_data = Fi::ClockData.new.data
phase = Fi::PhaseSignal.new(clock_data)

modulator = Fi::PhaseModulator.new 1/100.0
data = modulator.signal 10, phase

def output_filename(out_ext)
  ext = '.plot.rb'
  dir = File.dirname(__FILE__)
  base = File.basename(__FILE__, ext)

  File.join(dir, base + out_ext)
end


Gnuplot.open do |gp|
  Gnuplot::Plot.new(gp) do |plot|

    plot.output output_filename('.svg')
    plot.terminal 'svg'

    plot.xrange  '[6.98:7.1]'
    plot.yrange  '[-5:3]'
    plot.y2range '[-1:4]'
    plot.unset 'y2tics'

    plot.ytics 'nomirror'
    plot.xtics 'nomirror'

    plot.samples 1000

    t, output, phi = data.transpose

    signal = Gnuplot::DataSet.new([t, output]) do |ds|
      ds.title = 'Signal modulé'
      ds.with = 'lines'
    end

    sine = Gnuplot::DataSet.new('sin(2*pi*x*100)')

    phase = Gnuplot::DataSet.new([t, phi]) do |ds|
      ds.title = 'phase'
      ds.axes = 'x1y2'
      ds.with = 'lines'
    end

    plot.data = [signal, sine, phase]

  end
end


