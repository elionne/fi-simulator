module Fi

  # http://www.f-legrand.fr/scidoc/docmml/sciphys/electro/synthese/synthese.html
  # https://fr.wikipedia.org/wiki/Rotation_vectorielle#.C3.89criture_matricielle
  # https://fr.wikipedia.org/wiki/Matrice_de_rotation
  class Oscillator
    def initialize(phase = 0)
      @y = Math::sin(phase)
      @x = Math::cos(phase)
    end

    def run(f_normalized)
      cos_f = Math::cos(2*Math::PI*f_normalized)
      sin_f = Math::sin(2*Math::PI*f_normalized)

      [@x, @y]
    ensure
      @x, @y = cos_f*@x - sin_f*@y, sin_f*@x + cos_f*@y
    end

  end
end
