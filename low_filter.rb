module Fi
  class LowFilter
    attr_reader :signal
    attr_reader :x, :y

    def initialize
      # 2nd order factors
      @b = [1.1905e-04,   2.3809e-04,   1.1905e-04]
      @a = [1.00000   ,  -1.96890   ,   0.96938   ]

      # 1 order factors, [b, a] = butter(1, 0.0007)
      @b = [0.0010984,   0.0010984, 0]
      @a = [1.0000000,  -0.9978000, 0]

      # 1 order factors, [b, a] = butter(1, 0.00925)
      @b = [0.0071740,   0.0071740, 0]
      @a = [1.000000,  -0.98565, 0]

      @y = [0.00000,   0.00000,   0.00000]
      @x = [0.00000,   0.00000,   0.00000]
    end


    def filter(x)
      @x.unshift(x).pop
      @y.unshift(0).pop

      y = @b[0]*@x[0] + @b[1]*@x[1] + @b[2]*@x[2] \
                      - @a[1]*@y[1] - @a[2]*@y[2]

      @y[0] = y / @a[0]
    end

  end
end
