module Fi
  class Signal

    attr_reader :bin, :signal
    attr_accessor :hist_max, :hist_min

    attr_reader :edge

    alias :edge? :edge

    def initialize
      @bin = +1
      @hist_max = +0.1
      @hist_min = -0.1
    end

    def signal=(s)
      @signal = s

      @edge = case bin
      when +1 then s < hist_min ? @bin = -1 : 0
      when -1 then s > hist_max ? @bin = +1 : 0
      end
    end
  end

end
