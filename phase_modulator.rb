
module Fi

  class PhaseModulator

    attr_reader :period, :samples

    def initialize(period = 1e-3, samples = 20)
      @period = period
      @samples = samples
    end

    def sampling
      period / samples
    end

    def signal(duration, phase)
      data = []
      (duration / sampling).to_i.times do |i|
        data << signal_sample(i, phase)
      end

      data
    end

    def signal_sample(sample, phase)
      t = sample * sampling

      phi = phase.at(t)

      output = Math.sin( 2*Math::PI*t / period + phi )

      # puts "% 0.6f % 0.6f % 0.6f" % [t, output, phi].map{ |f| f.round 6 }
      [t, output, phi]
    end

  end
end
